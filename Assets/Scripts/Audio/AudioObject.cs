using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Audio Object", menuName = "Terra Forma/Audio/AudioObject")]
public class AudioObject : ScriptableObject
{
    public AudioObjectType type;

    [HorizontalGroup("Buttons")]
    [Button("Play", ButtonHeight = 30)]
    public void Preview()
    {
        EditorSFX.Play(this);
    }

    [HorizontalGroup("Buttons")]
    [Button("Stop", ButtonHeight = 30)]
    public void StopPreview()
    {
        EditorSFX.Stop();
    }

    [ShowIf("type", AudioObjectType.Music), HorizontalGroup("MusicLoop")]
    public float loopStart;
    [ShowIf("type", AudioObjectType.Music), HorizontalGroup("MusicLoop")]
    public float loopEnd;

    [HideIf("type", AudioObjectType.Music)]
    public List<RandomSFX> sfx;

    [ShowIf("type", AudioObjectType.Music)]
    public AudioClip clip;

    public RandomSFX GetRandomSFX()
    {
        return sfx[UnityEngine.Random.Range(0, sfx.Count)];
    }

    public bool ContainsSFXClip(AudioClip clip)
    {
        if (sfx == null || sfx.Count == 0)
            return false;

        for (int i = 0; i < sfx.Count; i++)
        {
            if (sfx[i].clip == clip)
                return true;
        }

        return false;
    }

    public RandomSFX GetSFX(AudioClip clip)
    {
        if (sfx == null || sfx.Count == 0)
            return null;

        for (int i = 0; i < sfx.Count; i++)
        {
            if (sfx[i].clip == clip)
                return sfx[i];
        }

        return null;
    }
}

[System.Serializable]
public class RandomSFX
{
    public AudioClip clip;
    [MinMaxSlider(minValue: 0, maxValue: 2, showFields: true)]
    public Vector2 pitch = Vector2.one;
    [MinMaxSlider(minValue: 0, maxValue: 1, showFields: true)]
    public Vector2 volume = Vector2.one;

    [HorizontalGroup("Buttons")]
    [Button("Play")]
    public void Preview()
    {
        if (clip != null)
        {
            StopPreview();
            EditorSFX.Play(this);
        }
    }

    [HorizontalGroup("Buttons")]
    [Button("Stop")]
    public void StopPreview()
    {
        EditorSFX.Stop();
    }
}

public enum AudioObjectType
{
    SFX,
    Ambiance,
    Music,
}
