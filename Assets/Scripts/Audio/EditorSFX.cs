using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections.Generic;

[ExecuteInEditMode]
public class EditorSFX : MonoBehaviour
{
    private static EditorSFX _instance;
    public static EditorSFX instance
    {
        get
        {
            if (_instance == null)
                Initialize();
            return _instance;
        }
    }
    public AudioSource preview;
    public List<AudioSource> tempPreview;
    public static void Initialize()
    {
        if (FindObjectOfType<EditorSFX>() != null)
        {
            _instance = FindObjectOfType<EditorSFX>();
            return;
        }

        GameObject go = new GameObject("EditorSFX");
        _instance = go.AddComponent<EditorSFX>();
        _instance.preview = go.AddComponent<AudioSource>();
        _instance.preview.playOnAwake = false;
        _instance.tempPreview = new List<AudioSource>();
    }

    public static void Stop()
    {
        instance.preview.Stop();
        ClearAmbiances();
    }

    public static void Play(RandomSFX randomSfx)
    {
        //Stop();
        instance.preview.clip = randomSfx.clip;
        instance.preview.volume = UnityEngine.Random.Range(randomSfx.volume.x, randomSfx.volume.y);
        instance.preview.pitch = UnityEngine.Random.Range(randomSfx.pitch.x, randomSfx.pitch.y);
        instance.preview.Play();
    }

    public static void Play(AudioClip clip)
    {
        //Stop();
        instance.preview.clip = clip;
        instance.preview.volume = 1;
        instance.preview.pitch = 1;
        instance.preview.Play();
    }

    public static void Play(AudioObject audioObject)
    {
        switch(audioObject.type)
        {
            case AudioObjectType.SFX:
                if (audioObject.sfx?.Count > 0)
                {
                    Play(audioObject.sfx[UnityEngine.Random.Range(0, audioObject.sfx.Count)]);
                }
                break;
            case AudioObjectType.Ambiance:
                PlayAmbiances(audioObject);
                break;
            default:
                if (audioObject.clip != null)
                    Play(audioObject.clip);
                break;
        }
    }

    static void ClearAmbiances()
    {
        if (instance.tempPreview.Count > 0)
        {
            List<AudioSource> toDelete = new List<AudioSource>();
            foreach (AudioSource source in instance.tempPreview)
            {
                toDelete.Add(source);
                source.Stop();
            }

            foreach (AudioSource source in toDelete)
            {
                DestroyImmediate(source);
            }
            instance.tempPreview = new List<AudioSource>();
        }
    }

    static void PlayAmbiances(AudioObject audioObject)
    {
        ClearAmbiances();
        foreach (RandomSFX rsfx in audioObject.sfx)
        {
            PlayAmbiance(rsfx);
        }
    }

    static void PlayAmbiance(RandomSFX ambiance)
    {
        AudioSource source = instance.gameObject.AddComponent<AudioSource>();
        source.clip = ambiance.clip;
        source.volume = ambiance.volume.x;
        source.pitch = ambiance.pitch.x;
        source.Play();
        instance.tempPreview.Add(source);
    }

    public static void PlayClip(AudioClip clip, int startSample = 0, bool loop = false)
    {
        Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;

        Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        MethodInfo method = audioUtilClass.GetMethod(
            "PlayPreviewClip",
            BindingFlags.Static | BindingFlags.Public,
            null,
            new Type[] { typeof(AudioClip), typeof(int), typeof(bool) },
            null
        );

        method.Invoke(
            null,
            new object[] { clip, startSample, loop }
        );
    }

    public static void StopAllClips()
    {
        Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;

        Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        MethodInfo method = audioUtilClass.GetMethod(
            "StopAllPreviewClips",
            BindingFlags.Static | BindingFlags.Public,
            null,
            new Type[] { },
            null
        );

        method.Invoke(
            null,
            new object[] { }
        );
    }
}

