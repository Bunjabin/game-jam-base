using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;

public class AudioManager : MonoBehaviour
{
    public AudioObject currentMusic, currentAmbiance;
    public Dictionary<AudioClip, AudioSource> ambiances;
    public Dictionary<AudioObject, AudioSource> musics;
    public List<SFXObject> sfxPlaying;
    public List<AudioSource> sfxPool;

    float fadeDuration = 0;

    float masterVolume = 1f;
    float musicVolume = 1f;
    float ambianceVolume = 1f;
    float sfxVolume = 1f;

    int sfxPoolTarget = 10;

    float GetVolumeSetting(string key)
    {
        return 1f;// return GameManager.instance.globalVariables.FetchValue("Audio Settings", key) * 0.01f;
    }

    public void Initialize()
    {
        RefreshAudioSettings();

        sfxPlaying = new List<SFXObject>();
        sfxPool = new List<AudioSource>();

        ambiances = new Dictionary<AudioClip, AudioSource>();
        musics = new Dictionary<AudioObject, AudioSource>();
    }

    public void RefreshAudioSettings()
    {
        masterVolume = GetVolumeSetting("Master Volume");
        musicVolume = GetVolumeSetting("Music Volume") * masterVolume;
        ambianceVolume = GetVolumeSetting("Ambiance Volume") * masterVolume;
        sfxVolume = GetVolumeSetting("SFX Volume") * masterVolume;
    }

    AudioSource CreateAudioSource(bool playOnAwake = false, AudioClip clip = null, bool loop = false, float volume = 1, float spatialBlend = 0f)
    {
        AudioSource newSource = gameObject.AddComponent<AudioSource>();
        if (clip != null)
        {
            newSource.loop = loop;
            newSource.clip = clip;
            newSource.playOnAwake = playOnAwake;
            newSource.volume = volume;
        }
        newSource.spatialBlend = spatialBlend;


        if (playOnAwake)
            newSource.Play();
        return newSource;
    }

    public void PlayAudio(AudioObject audio, float fadeDuration = 0)
    {
        if (audio == null)
            return;

        switch (audio.type)
        {
            case AudioObjectType.SFX: PlaySFX(audio); break;
            case AudioObjectType.Music: PlayMusic(audio, fadeDuration); break;
            case AudioObjectType.Ambiance: SetAmbiance(audio, fadeDuration); break;
        }
    }

    public void StopAudio(AudioObject audio, float fadeDuration = 0)
    {
        if (audio == null)
            return;

        switch (audio.type)
        {
            case AudioObjectType.Music: StopMusic(audio, fadeDuration); break;
        }
    }

    public void PlayMusic(AudioObject audio, float fadeDuration = 0)
    {
        Debug.Log("Play Music " + audio);
        if (audio == currentMusic || audio == null)
            return;

        if (fadeDuration == 0)
            StopMusic(null);

        if (!musics.ContainsKey(audio))
        {
            AudioSource source = CreateAudioSource(true, audio.clip, true, 0f);
            musics.Add(audio, source);
            Debug.Log("Creating new audio source");
        }

        if (fadeDuration > 0)
        {
            currentMusic = audio;
            TriggerMusicFade(fadeDuration);
        }
        else
        {
            currentMusic = audio;
            musics[audio].volume = musicVolume;
        }
        
    }

    public void StopMusic(AudioObject audio, float fadeDuration = 0)
    {
        if (fadeDuration <= 0)
        {
            if (audio == null)
            {
                //stop all immediately
                AudioObject[] audioObjects = musics.Keys.ToArray<AudioObject>();
                for (int i = 0; i < audioObjects.Length; i++)
                {
                    Destroy(musics[audioObjects[i]]);
                    musics.Remove(audioObjects[i]);
                }
            }
            else if (musics.ContainsKey(audio))
            {
                Destroy(musics[audio]);
                musics.Remove(audio);
            }
            currentMusic = null;
        }
        else
        {
            currentMusic = null;
            TriggerMusicFade(fadeDuration);
        }
    }

    public void SetAmbiance(AudioObject ambiance, float fadeDuration = 0)
    {
        currentAmbiance = ambiance;
        if (fadeAmbianceRoutine == null)
            fadeAmbianceRoutine = StartCoroutine(FadeAmbianceRoutine(fadeDuration));
    }

    public void PlaySFX(AudioObject audio)
    {
        if (audio == null)
            return;

        AudioSource source = PullSFXSource();
        SFXObject sfx = new SFXObject(audio, source);
        RandomSFX randSFX = audio.GetRandomSFX();
        sfx.audioSource.clip = randSFX.clip;
        sfx.audioSource.volume = Random.Range(randSFX.volume.x, randSFX.volume.y) * sfxVolume;
        sfx.audioSource.Play();
        sfxPlaying.Add(sfx);
    }

    public void StopSFX(AudioObject audio)
    {
        if (audio == null) //stop all sfx sounds
        {
            foreach(SFXObject sfx in sfxPlaying)
            {
                sfx.audioSource.Stop();
                sfx.audioSource.clip = null;
                sfxPool.Add(sfx.audioSource);
            }
            sfxPlaying.Clear();
        }
        else //stop all instances of audio
        {
            List<SFXObject> sourcesStopped = new List<SFXObject>();
            foreach (SFXObject sfx in sfxPlaying)
            {
                if (sfx.audioObject != audio)
                    continue;

                sfx.audioSource.Stop();
                sfx.audioSource.clip = null;
                sfxPool.Add(sfx.audioSource);
                sourcesStopped.Add(sfx);
            }
            
            foreach (SFXObject sfx in sourcesStopped)
            {
                sfxPlaying.Remove(sfx);
            }
        }
    }

    AudioSource PullSFXSource()
    {
        AudioSource source;
        if (sfxPool.Count > 0)
        {
            source = sfxPool[sfxPool.Count - 1];
            sfxPool.Remove(source);
        }
        else
        {
            source = CreateAudioSource(false, null, false, sfxVolume);
            if (SFXPoolCount() > sfxPoolTarget)
            {
                //set timeout until culling
                sfxCullCountdown += sfxCullWaitDuration;
            }
        }

        return source; ;
    }

    int SFXPoolCount()
    {
        return sfxPlaying.Count + sfxPool.Count;
    }

    Coroutine sfxPoolRoutine;
    float sfxCullCountdown = 0;
    const float sfxCullWaitDuration = 5;

    IEnumerator SFXCullCountdown()
    {
        while (SFXPoolCount() > sfxPoolTarget)
        {
            while (sfxCullCountdown > 0)
            {
                sfxCullCountdown -= Time.deltaTime;
                yield return null;
            }

            if (sfxPool.Count > 0)
            {
                int endCount = Mathf.Min(sfxPool.Count, SFXPoolCount() - sfxPoolTarget);
                for (int i = 0; i < endCount; i++)
                {
                    Destroy(sfxPool[i]);
                }
                sfxPool.RemoveRange(0, endCount);
            }
            if (SFXPoolCount() > sfxPoolTarget)
                sfxCullCountdown += sfxCullWaitDuration;
        }
    }

    Coroutine fadeMusicCoroutine, fadeAmbianceRoutine;

    void TriggerMusicFade(float fadeDuration)
    {
        if (fadeMusicCoroutine == null)
            fadeMusicCoroutine = StartCoroutine(FadeMusicRoutine(fadeDuration));
    }

    //Work towards currentMusic is at "full volume" and all other tracks fade to 0, and get deleted
    IEnumerator FadeMusicRoutine(float duration)
    {
        AudioObject[] audioObjects = musics.Keys.ToArray<AudioObject>();
        List<int> tracksToDelete = new List<int>();
        int hash = musics.GetHashCode();
        while ((currentMusic != null && (musics.Count != 1 || musics[currentMusic].volume != musicVolume)) || //fade in music
        (currentMusic == null && musics.Count > 0)) //fade out to stop music
        {
            if (musics.GetHashCode() != hash) //list has been modified
            {
                audioObjects = musics.Keys.ToArray<AudioObject>();
                hash = musics.GetHashCode();
            }

            for (int i = 0; i < audioObjects.Length; i++)
            {
                if (tracksToDelete.Contains(i))
                    continue;

                if (currentMusic == audioObjects[i])
                {
                    musics[audioObjects[i]].volume = Mathf.MoveTowards(musics[audioObjects[i]].volume, musicVolume, (Time.deltaTime * musicVolume) / duration);
                }
                else
                {
                    musics[audioObjects[i]].volume = Mathf.MoveTowards(musics[audioObjects[i]].volume, 0, (Time.deltaTime * musicVolume) / duration);
                    if (musics[audioObjects[i]].volume <= 0)
                    {
                        tracksToDelete.Add(i);
                        Destroy(musics[audioObjects[i]]);
                        musics.Remove(audioObjects[i]);
                    }
                }
            }

            yield return null;
        }

        fadeMusicCoroutine = null;
    }

    IEnumerator FadeAmbianceRoutine(float duration) //Fade out and delete all ambiance tracks no longer needed. Add and fade in all ambiance tracks that are
    {
        AudioClip[] audioClips = ambiances.Keys.ToArray();
        List<AudioClip> clipsToDelete = new List<AudioClip>();
        List<AudioClip> clipsToEnter = new List<AudioClip>();
        List <AudioClip > isDone = new List<AudioClip>();
        int hash = GetAmbianceHash();

        //Constantly check if done, or current ambiance has changed, if not, fade volume of all ambiances to target value
        while (!IsCurrentAmbianceSet(currentAmbiance, ref clipsToDelete, ref clipsToEnter))
        {
            //Debug.Log("Fading ambiances, " + clipsToEnter.Count + ", " + clipsToDelete.Count + "/" + audioClips.Length);
            if (GetAmbianceHash() != hash) //list has been modified
            {
                audioClips = ambiances.Keys.ToArray();
                hash = GetAmbianceHash();
            }

            for (int i = 0; i < audioClips.Length; i++)
            {
                if (clipsToEnter.Contains(audioClips[i]))
                {
                    RandomSFX sfx = currentAmbiance.GetSFX(audioClips[i]);
                    if (duration <= 0)
                        ambiances[audioClips[i]].volume = sfx.volume.x * ambianceVolume;
                    else
                        ambiances[audioClips[i]].volume = Mathf.MoveTowards(ambiances[audioClips[i]].volume, sfx.volume.x * ambianceVolume, (Time.deltaTime * sfx.volume.x * ambianceVolume) / duration);
                    if (ambiances[audioClips[i]].volume == sfx.volume.x * ambianceVolume) //reached target volume
                    {
                        isDone.Add(audioClips[i]);
                    }
                }
                else if (clipsToDelete.Contains(audioClips[i]))
                {
                    if (duration <= 0)
                        ambiances[audioClips[i]].volume = 0;
                    else
                        ambiances[audioClips[i]].volume = Mathf.MoveTowards(ambiances[audioClips[i]].volume, 0, (Time.deltaTime * ambianceVolume) / duration);
                    if (ambiances[audioClips[i]].volume <= 0)
                    {
                        Destroy(ambiances[audioClips[i]]);
                        ambiances.Remove(audioClips[i]);
                        isDone.Add(audioClips[i]);
                    }
                }
                //else clip is at correct volume
            }
            foreach (AudioClip ac in isDone)
            {
                if (clipsToEnter.Contains(ac))
                    clipsToEnter.Remove(ac);
                else if (clipsToDelete.Contains(ac))
                    clipsToDelete.Remove(ac);
            }
            isDone.Clear();
            yield return null;
        }
        fadeAmbianceRoutine = null;
    }

    int GetAmbianceHash()
    {
        int hash = 0;
        string s = "";
        foreach (AudioClip ac in ambiances.Keys)
        {
            s += ac.name;
        }
        return s.GetHashCode();
    }

    bool IsCurrentAmbianceSet(AudioObject newAmbiance, ref List<AudioClip> toDelete, ref List<AudioClip> clipsToEnter)
    {
        bool isDone = newAmbiance.sfx.Count == ambiances.Count;

        //Add all non-exsitant AudioClips as new AudioSources
        foreach (RandomSFX sfx in currentAmbiance.sfx)
        {
            if (!ambiances.ContainsKey(sfx.clip))
            {
                AudioClip ac = sfx.clip;
                ambiances.Add(sfx.clip, CreateAudioSource(true, sfx.clip, true, 0f));
                clipsToEnter.Add(ac);
            }
        }

        AudioClip[] keys = ambiances.Keys.ToArray();
        foreach (AudioClip key in keys) //find and mark redundant audioSources
        {
            if (newAmbiance.ContainsSFXClip(key)) //clip is still relevant
            {
                AudioSource source = ambiances[key];
                if (source.volume != newAmbiance.GetSFX(key).volume.x * ambianceVolume)
                {
                    if (!clipsToEnter.Contains(key))
                        clipsToEnter.Add(key);
                    isDone = false;
                }
            }
            else //clip is redundant
            {
                isDone = false;
                if (!toDelete.Contains(key))
                    toDelete.Add(key);
            }
        }
        return isDone;
    }
}

public struct SFXObject
{
    public AudioObject audioObject;
    public AudioSource audioSource;

    public SFXObject(AudioObject audioObject, AudioSource audioSource)
    {
        this.audioObject = audioObject;
        this.audioSource = audioSource;
    }
}
